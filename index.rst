.. Ansible tutorial documentation master file, created by
   sphinx-quickstart on Tue Mar 20 11:14:45 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/ansible/rss.xml>`_

.. _ansible_tuto:
.. _tuto_ansible:
.. _devops_ansible:

================================================================
**Ansible** provisioning--automation-configuration |ansible|
================================================================

- https://github.com/ansible/ansible
- https://docs.ansible.com/ansible/latest/index.html
- https://github.com/topics/ansible
- https://www.ansible.com/blog/topic/ansible
- https://forum.ansible.com/top?period=weekly
- https://landscape.cncf.io/guide?item=provisioning--automation-configuration--ansible#provisioning--automation-configuration

.. figure:: images/cncf_ansible.webp

   https://landscape.cncf.io/guide?item=provisioning--automation-configuration--ansible#provisioning--automation-configuration


.. toctree::
   :maxdepth: 3

   alternatives/alternatives
   installation/installation
   tutorials/tutorials
   books/books
   modules/modules
   roles/roles
   playbooks/playbooks
   use_cases/use_cases
   news/news
   glossaire/glossaire
   versions/versions
