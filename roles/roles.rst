

.. index::
   pair: Ansible ; Roles


.. _ansible_roles:

=============================================
Ansible roles
=============================================

.. seealso::

   - https://galaxy.ansible.com/list
   - https://docs.ansible.com/ansible/latest/playbooks_reuse_roles.html


.. toctree::
   :maxdepth: 3

   base/base
   python36/python36
   apache_http_server/apache_http_server
