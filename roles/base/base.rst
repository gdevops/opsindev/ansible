

.. index::
   pair: Base ; Role


.. _role_base:

=============================================
role base example
=============================================



roles/base/tasks/main.yml
=====================================

.. seealso::

   - :ref:`playbook_1`


::

    (ansible-yj-NDwFs) $ tree roles/base

::


	roles/base
	└── tasks
		└── main.yml


.. code-block:: yaml

	---
	# roles/base/tasks/main.yml
	# =========================
	# https://docs.ansible.com/ansible/yum_module.html
	- name: Installation des utilitaires de base
	  yum: name={{ item }} state=latest
	  become: true
	  with_items:
		- ntp
		- python-pip
		- net-tools
		- jwhois
		- fortune-mod
		- tree
		- git


	# https://docs.ansible.com/ansible/yum_module.html
	- name: Installation de Python SELinux library
	  yum: name=libselinux-python state=present


	# https://docs.ansible.com/ansible/service_module.html
	- name: ensure NTP is running
	  service: name=ntpd state=started enabled=yes

	# RHSCL (Red Hat Software Collections)
	# https://www.softwarecollections.org/en/
	- name: install redhat software collections
	  yum: name=centos-release-scl state=present



Running the base role
=========================


::

    ansible-playbook -i inventory/hosts -l staging playbook.yml


::

	 ________________
	< PLAY [staging] >
	 ----------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	 ________________________
	< TASK [Gathering Facts] >
	 ------------------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	ok: [staging]
	 ____________________________________________________
	< TASK [base : Installation des utilitaires de base] >
	 ----------------------------------------------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	ok: [staging] => (item=['ntp', 'python-pip', 'net-tools', 'jwhois', 'fortune-mod', 'tree', 'git'])
	 ______________________________________________________
	< TASK [base : Installation de Python SELinux library] >
	 ------------------------------------------------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	ok: [staging]
	 _____________________________________
	< TASK [base : ensure NTP is running] >
	 -------------------------------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	ok: [staging]
	 ___________________________________________________
	< TASK [base : install redhat software collections] >
	 ---------------------------------------------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	ok: [staging]
	 ____________
	< PLAY RECAP >
	 ------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	staging                    : ok=5    changed=0    unreachable=0    failed=0
