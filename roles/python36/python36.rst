

.. index::
   pair: Python36 ; Role


.. _role_python36:

=============================================
role python36 example
=============================================



roles/python36/tasks/main.yml
=====================================

.. seealso::

   - :ref:`playbook_1`


::

    (ansible-yj-NDwFs) $ tree roles/python36

::


	roles/python36
	└── tasks
		└── main.yml


.. code-block:: yaml

	---
	# roles/python36/tasks/main.yml
	# ==============================

	# # https://docs.ansible.com/ansible/yum_module.html
	- name: Installation de ius-release.rpm pour Python3.6
	  yum:
		name: https://centos7.iuscommunity.org/ius-release.rpm
		state: installed
	  become: true

	# https://docs.ansible.com/ansible/yum_module.html
	- name: Installation de Python3.6 devel pip setuptools
	  yum: name={{ item }} state=installed
	  become: true
	  with_items:
		- python36u
		- python36u-devel
		- python36u-pip
		- python36u-setuptools


Running the python36 role
==========================


::

    ansible-playbook -i inventory/hosts -l staging playbook.yml


::

	 _______________________________________________________
	/ TASK [python36 : Installation de ius-release.rpm pour \
	\ Python3.6]                                            /
	 -------------------------------------------------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	changed: [staging]
	 ______________________________________________________
	/ TASK [python36 : Installation de Python3.6 devel pip \
	\ setuptools]                                          /
	 ------------------------------------------------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	changed: [staging] => (item=['python36u', 'python36u-devel', 'python36u-pip', 'python36u-setuptools'])

	 ____________
	< PLAY RECAP >
	 ------------
			\   ^__^
			 \  (oo)\_______
				(__)\       )\/\
					||----w |
					||     ||

	staging                    : ok=7    changed=2    unreachable=0    failed=0
