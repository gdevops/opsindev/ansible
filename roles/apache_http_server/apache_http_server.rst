
.. index::
   pair: Apache http server ; Role


.. _role_apache_http_server:

=============================================
role apache http server
=============================================



roles/apache_http_server/tasks/main.yml
==========================================

.. seealso::

   - :ref:`playbook_1`



.. code-block:: yaml

    ---
    # roles/http_apache_server/tasks/main.yml
    # ========================================

    # https://docs.ansible.com/ansible/yum_module.html
    - name: Installation du serveur HTTP apache (https://en.wikipedia.org/wiki/Apache_HTTP_Server)
      yum: name={{ item }} state=present
      with_items:
        - httpd
        - httpd-devel

    # https://docs.ansible.com/ansible/service_module.html
    - name: Start and enable Apache HTTP server
      service: name=httpd  state=started enabled=yes



Running the apache http server role
======================================


::

    ansible-playbook -i inventory/hosts -l staging playbook.yml


::

     ____________________________________________________________
    / TASK [http_apache_server : Installation du serveur HTTP    \
    \ apache (https://en.wikipedia.org/wiki/Apache_HTTP_Server)] /
     ------------------------------------------------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||

    changed: [staging] => (item=['httpd', 'httpd-devel'])
     _________________________________________________________
    / TASK [http_apache_server : Start and enable Apache HTTP \
    \ server]                                                 /
     ---------------------------------------------------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||

    changed: [staging]

     ____________
    < PLAY RECAP >
     ------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||

    staging                    : ok=9    changed=2    unreachable=0    failed=0
