

.. index::
   pair: win_file ; Module


.. _win_file_module:

======================================================================================
win_file - Creates, touches or removes files or directories
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/win_file_module.html
   - :ref:`copy_module`





Synopsis
==========


- Sets attributes of files, symlinks, and directories, or removes
  files/symlinks/directories. Many other modules support the same
  options as the file module - including copy, template, and assemble.
- For Windows targets, use the win_file module instead.
