

.. index::
   pair: shell ; Module


.. _shell_module:

=============================================
shell - Execute commands in nodes
=============================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/shell_module.html
   - :ref:`win_shell_module`
   - :ref:`command_module`





Synopsis
==========


- The shell module takes the command name followed by a list of
  space-delimited arguments. It is almost exactly like the command
  module but runs the command through a shell (/bin/sh) on the remote node.
- For Windows targets, use the win_shell module instead.
