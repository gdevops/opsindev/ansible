

.. index::
   pair: mysql_db ; Module
   pair: mysql_db ; MariaDB


.. _mysql_db_module:

======================================================================================
mysql_db - Add or remove MySQL databases from a remote host
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/mysql_db_module.html




Synopsis
==========

- Add or remove MySQL databases from a remote host.
