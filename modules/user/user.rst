

.. index::
   pair: Module ; user


.. _user_module:

======================================================================================
user - Manage user accounts
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/modules/user_module.html
   - https://github.com/ansible/ansible/blob/devel/lib/ansible/modules/system/user.py
   - https://docs.ansible.com/ansible/latest/modules/win_user_module.html#win-user




Synopsis
==========

- Manage user accounts and user attributes.
- For Windows targets, use the win_user_ module instead.


.. _win_user:  https://docs.ansible.com/ansible/latest/modules/win_user_module.html#win-user
