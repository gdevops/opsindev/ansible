

.. index::
   pair: win_unzip ; unarchive


.. _win_unzip_module:

======================================================================================
win_unzip - Unzips compressed files and archives on the Windows node
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/win_unzip_module.html
   - :ref:`unarchive_module`





Synopsis
==========

- Unzips compressed files and archives.
- Supports .zip files natively
- Supports other formats supported by the Powershell Community Extensions
  (PSCX) module (basically everything 7zip supports)
- For non-Windows targets, use the unarchive module instead.
