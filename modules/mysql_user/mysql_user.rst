

.. index::
   pair: mysql_user ; Module
   pair: mysql_user ; MariaDB


.. _mysql_user_module:

======================================================================================
mysql_user - Adds or removes a user from a MySQL database
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/mysql_user_module.html




Synopsis
==========

- Adds or removes a user from a MySQL database.
