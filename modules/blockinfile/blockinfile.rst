

.. index::
   pair: blockinfile ; Module


.. _blockinfile_module:

==============================================================================
blockinfile - Insert/update/remove a text block surrounded by marker lines
==============================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/blockinfile_module.html
   - :ref:`replace_module`
   - :ref:`lineinfile_module`





Synopsis
==========

- This module will insert/update/remove a block of multi-line text
  surrounded by customizable marker lines
