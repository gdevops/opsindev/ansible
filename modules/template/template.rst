

.. index::
   pair: template ; Module


.. _template_module:

======================================================================================
template - Templates a file out to a remote server
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/template_module.html





Synopsis
==========

- Templates are processed by the Jinja2 templating language
  (http://jinja.pocoo.org/docs/) - documentation on the template formatting
  can be found in the Template Designer Documentation (http://jinja.pocoo.org/docs/templates/).

- Six additional variables can be used in templates: ansible_managed
  (configurable via the defaults section of ansible.cfg) contains a
  string which can be used to describe the template name, host,
  modification time of the template file and the owner uid.

  - template_host contains the node name of the template’s machine.
  - template_uid the numeric user id of the owner.
  - template_path the path of the template.
  - template_fullpath is the absolute path of the template.
  - template_run_date is the date that the template was rendered.
