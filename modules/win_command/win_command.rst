

.. index::
   pair: win_command ; Module


.. _win_command_module:

===========================================================
win_command - Executes a command on a remote Windows node
===========================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/win_command_module.html
   - :ref:`command_module`





Synopsis
==========


- The win_command module takes the command name followed by a list of
  space-delimited arguments.
- The given command will be executed on all selected nodes.
  It will not be processed through the shell, so variables like
  $env:HOME and operations like "<", ">", "|", and ";" will not work
  (use the :ref:`win_shell module <win_shell_module>` if you need these features).
- For non-Windows targets, use the :ref:`command module <command_module>`
  instead.
