

.. index::
   pair: lineinfile ; Module


.. _lineinfile_module:

============================================================================================================================
lineinfile - Ensure a particular line is in a file, or replace an existing line using a back-referenced regular expression
============================================================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/lineinfile_module.html
   - :ref:`replace_module`
   - :ref:`blockinfile_module`




Synopsis
==========

- This module will search a file for a line, and ensure that it is
  present or absent.
- This is primarily useful when **you want to change a single line in a
  file only**.
  See:

  - the :ref:`replace module <replace_module>` if you want to change
    multiple, similar lines
  - or check blockinfile if you want to insert/update/remove a block
    of lines in a file.

  For other cases, see the :ref:`copy <copy_module>` or :ref:`template <template_module>`
  modules.
