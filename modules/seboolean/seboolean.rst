

.. index::
   pair: seboolean ; Module
   pair: selinux ; GNU/Linux


.. _seboolean_module:

======================================================================================
seboolean - Toggles SELinux booleans.
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/seboolean_module.html
   - https://en.wikipedia.org/wiki/Security-Enhanced_Linux




Synopsis
==========


- Toggles SELinux booleans.


Example
=========

.. code-block:: yaml

   # Set (httpd_can_network_connect) flag on and keep it persistent across reboots
   - seboolean:
       name: httpd_can_network_connect
       state: yes
       persistent: yes
