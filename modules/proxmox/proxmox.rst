

.. index::
   pair: Module ; proxmox


.. _proxmox_module:

======================================================================================
proxmox - management of instances in Proxmox VE cluster
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/proxmox_module.html




Synopsis
==========

- allows you to create/delete/stop instances in Proxmox VE cluster
- Starting in Ansible 2.1, it automatically detects containerization
  type (lxc for PVE 4, openvz for older)
