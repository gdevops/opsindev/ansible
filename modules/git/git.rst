

.. index::
   pair: Module ; git


.. _git_module:

======================================================================================
git - Deploy software (or files) from git checkouts
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/modules/git_module.html
   - https://github.com/ansible/ansible/blob/devel/lib/ansible/modules/source_control/git.py




Synopsis
==========


- Manage git checkouts of repositories to deploy files or software.



How do I pass username and password while using Ansible Git module ?
=======================================================================

.. seealso::

   - https://stackoverflow.com/questions/37841914/how-do-i-pass-username-and-password-while-using-ansible-git-module#37851105


.. _ansible_git_credential_cache:

git config --global credential.helper cache (without timeout)
--------------------------------------------------------------

To not continuously type the git username and password::

    git config --global credential.helper cache


.. _ansible_git_credential_cache_with_timeout:

git config --global credential.helper 'cache --timeout=28800'
----------------------------------------------------------------

Pour ne pas avoir à taper son username/password à chaque push ou pull.

::

    git config --global credential.helper 'cache --timeout=28800'
