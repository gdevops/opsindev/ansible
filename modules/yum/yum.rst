

.. index::
   pair: yum ; Module
   pair: yum ; CentOS
   pair: yum ; package manager


.. _yum_module:

=====================================================================================
yum (Yellowdog Updater, Modified ) - Manages packages with the yum package manager
=====================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/yum_module.html
   - https://en.wikipedia.org/wiki/Yellow_Dog_Updater,_Modified




Synopsis
==========


- Installs, upgrade, downgrades, removes, and lists packages and groups
  with the yum package manager.
