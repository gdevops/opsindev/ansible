.. index::
   pair: command ; Module

.. _command_module:

====================================================
**command** - Executes a command on a remote node
====================================================


- https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html
- :ref:`shell_module`

Synopsis
==========

- The command module takes the command name followed by a list of
  space-delimited arguments.
- The given command will be executed on all selected nodes.
  It will not be processed through the shell, so variables like
  $HOME and operations like "<", ">", "|", ";" and "&" will not work
  (use the shell module if you need these features).
- For Windows targets, use the win_command module instead.
