

.. index::
   pair: selinux ; Module
   pair: selinux ; GNU/Linux


.. _selinux_module:

======================================================================================
selinux (Security-Enhanced Linux) - Change policy and state of SELinux
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/selinux_module.html
   - https://en.wikipedia.org/wiki/Security-Enhanced_Linux




Synopsis
==========


- Configures the SELinux mode and policy. A reboot may be required after
  usage.
  Ansible will not issue this reboot but will let you know when it is required.
