

.. index::
   pair: win_service ; Module
   pair: win_service ; Windows


.. _winservice_module:

======================================================================================
win_service - Manage services
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/win_service_module.html#win-service




Synopsis
==========

- Manages Windows services.
- For non-Windows targets, use the :ref:`service module instead <winservice_module>`.
