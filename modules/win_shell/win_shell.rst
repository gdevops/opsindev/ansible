

.. index::
   pair: win_shell ; Module
   pair: win_shell ; Windows
   pair: win_shell ; PowerShell


.. _win_shell_module:

======================================================
win_shell - Execute shell commands on target hosts
======================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/win_shell_module.html
   - :ref:`shell_module`





Synopsis
==========

- The win_shell module takes the command name followed by a list of
  space-delimited arguments. It is similar to the :ref:`win_command module <win_command_module>`,
  but runs the command via a shell (defaults to PowerShell) on the target host.
- For non-Windows targets, use the :ref:`shell module <shell_module>`
  instead.
