

.. index::
   pair: win_ping ; Module
   pair: win_ping ; Windows


.. _winping_module:

======================================================================================
win_ping - A windows version of the classic ping module
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/win_ping_module.html
   - :ref:`ping_module`




Synopsis
==========

- Checks management connectivity of a windows host.
- This is NOT ICMP ping, this is just a trivial test module.
- For non-Windows targets, use the ping module instead.
