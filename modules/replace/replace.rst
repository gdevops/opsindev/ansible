

.. index::
   pair: replace ; Module


.. _replace_module:

============================================================================================================================
replace - Replace all instances of a particular string in a file using a back-referenced regular expression
============================================================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/replace_module.html
   - :ref:`lineinfile_module`





Synopsis
==========

- This module will replace all instances of a pattern within a file.
- It is up to the user to maintain idempotence by ensuring that the
  same pattern would never match any replacements made
