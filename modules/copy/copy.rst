

.. index::
   pair: copy ; Module


.. _copy_module:

======================================================================================
copy - Copies files to remote locations
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/copy_module.html
   - :ref:`win_copy_module`




Synopsis
==========

- The copy module copies a file from the local or remote machine to a
  location on the remote machine. Use the fetch module to copy files
  from remote locations to the local box.
  If you need variable interpolation in copied files, use the template module.
- For Windows targets, use the :ref:`win_copy module <win_copy_module>` instead.
