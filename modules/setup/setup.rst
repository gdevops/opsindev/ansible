

.. index::
   pair: setup ; Module


.. _setup_module:

======================================================================================
setup - Gathers facts about remote hosts
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/setup_module.html





Synopsis
==========

- This module is automatically called by playbooks to gather useful
  variables about remote hosts that can be used in playbooks.
  It can also be executed directly by /usr/bin/ansible to check what
  variables are available to a host. Ansible provides many facts about
  the system, automatically.
- This module is also supported for Windows targets.


_setup_example:

setup example
===============


.. code-block:: yaml

	# Display facts from all hosts and store them indexed by I(hostname) at C(/tmp/facts).
	ansible all -i staging/hosts -m setup --tree /tmp/facts
