

.. index::
   pair: Ping ; Modules


.. _ping_module:

======================================================================================
ping - Try to connect to host, **verify a usable python** and return pong on success
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/ping_module.html




Synopsis
==========

A trivial test module, this module always returns pong on successful
contact.

It does not make sense in playbooks, but it is useful from /usr/bin/ansible
to verify the ability to login and that a usable python is configured.

This is **NOT ICMP ping**, this is just a trivial test module.
For Windows targets, use the win_ping module instead.
