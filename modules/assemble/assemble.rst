

.. index::
   pair: assemble ; Module


.. _assemble_module:

======================================================================================
assemble - Assembles a configuration file from fragments
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/assemble_module.html
   - :ref:`file_module`




Synopsis
==========

- Assembles a configuration file from fragments.

  Often a particular program will take a single configuration
  file and does not support a conf.d style structure where it is easy
  to build up the configuration from multiple sources.

  assemble will take a directory of files that can be local or have
  already been transferred to the system, and concatenate them together
  to produce a destination file.

  Files are assembled in string sorting order.

  Puppet calls this idea fragments.
