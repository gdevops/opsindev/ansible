

.. index::
   pair: stat ; Module


.. _stat_module:

==============================================================================
stat - Retrieve file or file system status
==============================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/stat_module.html





Synopsis
==========

- Retrieves facts for a file similar to the linux/unix ‘stat’ command.
- For Windows targets, use the win_stat module instead.
