

.. index::
   pair: debug ; Module


.. _debug_module:

======================================================================================
debug - Print statements during execution
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/debug_module.html




Synopsis
==========


- This module prints statements during execution and can be useful for
  debugging variables or expressions without necessarily halting the
  playbook.
  Useful for debugging together with the ‘when:’ directive.
- This module is also supported for Windows targets.
