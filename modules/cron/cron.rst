

.. index::
   pair: cron ; Module


.. _cron_module:

========================================================================
cron - Manage cron.d and crontab entries
========================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/modules/cron_module.html
   - https://github.com/ansible/ansible/blob/devel/lib/ansible/modules/system/cron.py





Synopsis
==========


- Use this module to manage crontab and environment variables entries.
  This module allows you to create environment variables and named
  crontab entries, update, or delete them.
- When crontab jobs are managed: the module includes one line with the
  description of the crontab entry *#Ansible: <name>* corresponding to
  the *name* passed to the module, which is used by future ansible/module
  calls to find/check the state.

  The *name* parameter should be unique, and changing the *name* value
  will result in a new cron task being created (or a different one being
  removed).
- When environment variables are managed: no comment line is added, but,
  when the module needs to find/check the state, it uses the *name*
  parameter to find the environment variable definition line.
- When using symbols such as %, they must be properly escaped.
