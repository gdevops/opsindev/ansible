

.. index::
   pair: Module ; git config


.. _git_config_module:

======================================================================================
git_config - Read and write git configuration
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/modules/git_config_module.html
   - https://github.com/ansible/ansible/blob/devel/lib/ansible/modules/source_control/git_config.py




Synopsis
==========

The git_config module changes git configuration by invoking **git config**.
This is needed if you don’t want to use template for the entire git
config file (e.g. because you need to change just user.email
in /etc/.git/config).

Solutions involving command are cumbersone or don’t work correctly
in check mode.
