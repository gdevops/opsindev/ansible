

.. index::
   pair: file ; Module


.. _file_module:

======================================================================================
file - Sets attributes of files
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/file_module.html
   - :ref:`win_file_module`
   - :ref:`copy_module`
   - :ref:`assemble_module`





Synopsis
==========


- Sets attributes of files, symlinks, and directories, or removes
  files/symlinks/directories.

  Many other modules support the same options as the file module,
  including :ref:`copy <copy_module>`, :ref:`template <template_module>`,
  and :ref:`assemble <assemble_module>`.

- For Windows targets, use the :ref:`win_file module <win_file_module>` instead.
