

.. index::
   pair: pip ; Module
   pair: pip ; Python


.. _pip_module:

======================================================================================
pip - Manages Python library dependencies
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/pip_module.html




Synopsis
==========

- Manage Python library dependencies. To use this module, one of the
  following keys is required: name or requirements.
