

.. index::
   pair: win_copy ; Module


.. _win_copy_module:

======================================================================================
win_copy - Copies files to remote locations on windows hosts
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/win_copy_module.html
   - :ref:`copy_module`





Synopsis
==========

- The win_copy module copies a file on the local box to remote windows
  locations.
- For non-Windows targets, use the :ref:`copy module <copy_module>` instead.
