

.. index::
   pair: Module ; unarchive


.. _unarchive_module:

======================================================================================
unarchive - Unpacks an archive after (optionally) copying it from the local machine
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/unarchive_module.html
   - :ref:`win_unzip_module`




Synopsis
==========



- The unarchive module unpacks an archive.
- By default, it will copy the source file from the local system to
  the target before unpacking.
- Set remote_src=yes to unpack an archive which already exists on the target.
- For Windows targets, use the :ref:`win_unzip <win_unzip_module>` module instead.
