

.. index::
   pair: get_url ; Module


.. _get_url_module:

======================================================================================
get_url - Print statements during execution
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/modules/get_url_module.html#get-url




Synopsis
==========

- Downloads files from HTTP, HTTPS, or FTP to the remote server.
  The remote server must have direct access to the remote resource.
- By default, if an environment variable <protocol>_proxy is set on the
  target host, requests will be sent through that proxy.
  This behaviour can be overridden by setting a variable for this task
  (see setting the environment), or by using the use_proxy option.
- HTTP redirects can redirect from HTTP to HTTPS so you should be sure
  that your proxy environment for both protocols is correct.
- From Ansible 2.4 when run with –check, it will do a HEAD request to
  validate the URL but will not download the entire file or verify it
  against hashes.
- For Windows targets, use the win_get_url module instead
