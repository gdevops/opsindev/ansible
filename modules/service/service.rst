

.. index::
   pair: service ; Module
   pair: service ; GNU/Linux


.. _service_module:

======================================================================================
service - Manage services
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/service_module.html




Synopsis
==========


- Controls services on remote hosts. Supported init systems include
  BSD init, OpenRC, SysV, Solaris SMF, systemd, upstart.
- For Windows targets, use the win_service module instead.
