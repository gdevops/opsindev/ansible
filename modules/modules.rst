

.. index::
   pair: Ansible ; Modules


.. _ansible_modules:

========================
Ansible modules
========================

.. seealso::

   - https://docs.ansible.com/ansible/latest/list_of_all_modules.html

.. toctree::
   :maxdepth: 3


   assemble/assemble
   blockinfile/blockinfile
   command/command
   copy/copy
   cron/cron
   debug/debug
   file/file
   get_url/get_url
   git/git
   git_config/git_config
   lineinfile/lineinfile
   mysql_db/mysql_db
   mysql_user/mysql_user
   ping_pong/ping_pong
   pip/pip
   proxmox/proxmox
   replace/replace
   rpm_key/rpm_key
   seboolean/seboolean
   selinux/selinux
   service/service
   setup/setup
   shell/shell
   stat/stat
   template/template
   unarchive/unarchive
   user/user
   win_command/win_command
   win_copy/win_copy
   win_file/win_file
   win_ping/win_ping
   win_shell/win_shell
   win_service/win_service
   win_unzip/win_unzip
   yum/yum
