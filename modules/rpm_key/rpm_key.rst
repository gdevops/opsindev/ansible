

.. index::
   pair: rpm_key ; Module
   pair: rpm_key ; Centos7


.. _rpm_key_module:

======================================================================================
rpm_key - Adds or removes a gpg key from the rpm db
======================================================================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/rpm_key_module.html




Synopsis
==========

- Adds or removes (rpm –import) a gpg key to your rpm database.
