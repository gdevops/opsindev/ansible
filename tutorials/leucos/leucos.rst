.. index::
   pair: Leucos ; Ansible tutorial

.. _leucos_tutorial:

===========================================
Leucos Ansible tutorial
===========================================

.. seealso::

   - https://github.com/leucos/ansible-tuto



Step 01
=========

.. seealso::

   - https://github.com/leucos/ansible-tuto/tree/master/step-01
