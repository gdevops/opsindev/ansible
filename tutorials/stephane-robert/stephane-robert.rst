
.. index::
   pair: Stéphane Robert ; Ansible


.. _ansible_stephane_robert:

==========================================================
Tutoriel de Stéphane Robert sur **Ansible** |ansible|
==========================================================

- https://blog.stephane-robert.info/docs/infra-as-code/gestion-de-configuration/ansible/introduction/

.. _intro_ansible:

Introduction
=============

Ansible est devenu un outil incontournable de nos jours, il suffit de regarder
le nombre d'offres d'emplois exigeant sa connaissance.
Conçu pour être simple et efficace, il se distingue par sa capacité à simplifier
des processus complexes, rendant l'Automatisation accessible même aux débutants.

Sa création, remontant à 2012, fut motivée par le besoin d'un outil qui pourrait
non seulement faciliter la vie des administrateurs système, mais aussi être
facilement compris par ceux qui débutent dans le domaine de l'automatisation.

Ce qui rend Ansible particulièrement attrayant, c'est sa polyvalence et sa
facilité d'utilisation.

Que ce soit pour le déploiement d'applications, la gestion de configuration
ou l'orchestration de tâches complexes, Ansible offre une solution élégante
et intuitive.


Cas d'Usage d'Ansible
==========================

- https://blog.stephane-robert.info/docs/infra-as-code/gestion-de-configuration/ansible/introduction/#cas-dusage-dansible

Dans ce billet, nous n'aborderons qu'Ansible.

Ansible fait partie d'un ensemble d'application appelé **Ansible Automation Platform**
qui étend son champ d'action à la gestion de WorkFlows Complexes, mais pas que.

En effet, Ansible offre une grande flexibilité et s'adapte à divers scénarios.

Ses applications vont de:

- l'automatisation de déploiements
- à la gestion de configurations,
- en passant par l'orchestration de tâches
- et la gestion d'événements.

Automatisation des Déploiements
-------------------------------------

Ansible est largement utilisé pour automatiser les déploiements d'applications
et de services.
Il permet aux équipes de réduire les erreurs humaines et d'accélérer les
processus de déploiement, grâce à une automatisation cohérente et fiable.

Gestion de Configuration
-------------------------------

La gestion de configuration avec Ansible assure une cohérence et une répétabilité
des configurations des serveurs et des applications à travers différents
environnements.
Cette approche unifiée aide à maintenir l'intégrité et la stabilité des
systèmes informatiques.

Orchestration
-----------------

**L'orchestration de tâches IT complexes** est facilitée par Ansible.

Il permet de définir avec précision l'ordre et la manière d'exécution des
tâches, optimisant ainsi la gestion de workflows complexes dans les
environnements IT.

Gestion des Événements
--------------------------

Un aspect innovant de l'utilisation d'Ansible est sa capacité à gérer les
événements de manière "event-driven".

Cette approche permet à Ansible de réagir automatiquement à des événements
spécifiques dans l'environnement IT.
Par exemple, Ansible peut être configuré  pour répondre à des changements
de configuration, à des alertes de sécurité, ou à d'autres déclencheurs dans
le système.

Cette réactivité rend les systèmes gérés par Ansible non seulement plus efficaces,
mais aussi plus adaptatifs aux conditions changeantes, un atout clé dans les
environnements IT dynamiques d'aujourd'hui.

.. _ansible_fonctionnement:

Comment fonctionne Ansible ?
==================================

- https://blog.stephane-robert.info/docs/infra-as-code/gestion-de-configuration/ansible/introduction/#comment-fonctionne-ansible-

Contrairement aux idées reçues pour apprendre à développer du code Ansible,
il n'y a pas besoin de déployer un serveur Ansible !

La grande force d'Ansible est qu’il est facile à mettre en œuvre, car il est
agent-less, et ne nécessite qu'une connexion SSH et la présence d'une installation
minimale de python pour exécuter les tâches décrites.

Lorsqu’il y a plusieurs machines à gérer, Ansible exécute les opérations en
parallèle. Cela permet de gagner un temps considérable.
Cependant, les tâches sont effectuées dans un ordre défini par l’utilisateur
lors du choix de la stratégie : Par défaut Ansible attendra d’avoir fini
une tâche (sur tous les hôtes) pour passer à la suivante.

Composants clés
======================

- **Node Manager** : ou control node, est le poste depuis lequel tout est exécuté
  via des connexions, essentiellement en SSH, aux nœuds cibles.
- Playbook : Un playbook Ansible décrit une suite de tâches ou de rôles écrits
  dans un fichier ou format YAML.
- Rôle : Afin d’éviter d’écrire encore et encore le même code dans les playbooks,
  Ansible permet d'utiliser des librairies regroupant des fonctionnalités spécifiques.
  Ces librairies sont appelées des rôles qui peuvent donc être utilisés dans
  les playbooks.
- Inventory : La description des systèmes cibles gérés par Ansible est appelé
    un inventaire.
    A chaque node de l'inventaire, on peut attribuer des variables.
    On distingue deux types d’inventaire :

    - l’inventaire statique constitué de fichier(s) plats.
    - l’inventaire dynamique fourni par un système centralisé.
      Exemple L'inventaire AWS EC2.
- Module : Les tâches et les rôles font appel à des modules installés avec Ansible.
  Je vous invite à consulter leur liste sur `le site d’Ansible <https://docs.ansible.com/ansible/latest/collections/index_module.html>`_.
- Template : Comme son nom l’indique, un template est un modèle permettant
  de générer un fichier cible. Ansible utilise Jinja2, un gestionnaire de modèles
  écrit pour Python.
  Les « Templates » Jinja2 permettent de gérer des boucles, des tests logiques,
  des listes ou des variables.
- Notifier : indique que si une tâche change d'état (et uniquement si la tâche
  a engendré un changement), notify fait appel au handler associé pour exécuter
  une autre tâche.
- Handler : Tâche qui n'est appelée que dans l'éventualité où un notifier est invoqué
- Tag : Nom défini sur une ou plusieurs tâches qui peuvent être utilisé plus
  tard pour exécuter exclusivement cette ou ces tâches Ansible.

Comment utiliser Ansible ?
Les principales commandes Ansible

Ansible est livré avec un certain nombre de commandes permettant soit de
lancer des actions ou, soit d'obtenir de l'information sur l'environnement
d'exécution Ansible :

- **ansible** : Permet d'exécuter un simple module ansible sur un inventaire.
  Vu ci-dessus dans le :ref:`premier test <test_ansible>`.
- **ansible-console** : Ouvre une console interactive permettant de lancer
  plusieurs actions sur un inventaire.
- ansible-config : Affiche l'ensemble des paramètres Ansible

  ::

      ansible-config [list|dump|view]:

  - list : affiche la liste complète des options d'Ansible à disposition.
  - dump : affiche la configuration dans le contexte actuel.
  - view : affiche le contenu d'un fichier de configuration Ansible

- **ansible-playbook** : Exécute un playbook Ansible sur un inventaire. **La plus connue**.
- ansible-vault : Permet de chiffrer des données qui ne doivent être divulgué.
- ansible-inventory : Affiche l'ensemble des données d'un inventaire Ansible.
- ansible-galaxy : permet d'installer des roles et des collections Ansible
- ansible-doc : Permet de lister l'ensemble des composants Ansible à disposition
  sur le nœud d'execution::

      ansible-doc -l -t [type]

  type parmi: become, cache, callback, cliconf,
  connection,httpapi, inventory, lookup, netconf, shell, vars, module, strategy.

