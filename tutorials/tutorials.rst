.. index::
   pair: Tutorials ; Ansible

.. _ansible_tutorials:

===========================================
Ansible tutorials
===========================================


.. toctree::
   :maxdepth: 3

   getting_started/getting_started
   my/my
   leucos/leucos
   ansible_for_devops/ansible_for_devops
   stephane-robert/stephane-robert
