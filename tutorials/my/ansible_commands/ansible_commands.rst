
.. index::
   pair: Ansible ; Commands


.. _ansible_commands:

===========================
Ansible command examples
===========================





Prerequisite
=============

inventory/hosts
---------------

::

    staging ansible_host=X.X.X.X ansible_user=root



ansible -m ping all -i inventory/hosts
======================================

.. seealso::

   - :ref:`ping_module`



::

    (ansible-yj-NDwFs) ansible -m ping all -i inventory/hosts

::

	staging | SUCCESS => {
		"changed": false,
		"ping": "pong"
	}


ansible -i inventory/hosts -m shell -a 'uname -a' all
=====================================================

.. seealso::

   - https://kernelnewbies.org/Linux_3.10
   - :ref:`shell_module`


On s'aperçoit que le noyau de cette machine centos7 date du 30 juin 2013 !


::

    (ansible-yj-NDwFs) ansible -i inventory/hosts -m shell -a 'uname -a' all

::

	staging | SUCCESS | rc=0 >>
	Linux transaction-staging 3.10.0-693.2.2.el7.x86_64 #1 SMP Tue Sep 12 22:26:13 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux


ansible staging -m setup -i inventory/hosts
=============================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/playbooks_variables.html#information-discovered-from-systems-facts
   - :ref:`setup_module`


::

     ansible staging -m setup -i inventory/hosts
