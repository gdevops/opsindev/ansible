
.. index::
   pair: Ansible ; Tutorial


.. _ansible_tutorial:

========================
My Ansible tutorial
========================


.. toctree::
   :maxdepth: 3

   ansible_commands/ansible_commands
   ansible_playbook_commands/ansible_playbook_commands
