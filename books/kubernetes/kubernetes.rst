.. index::
   pair: Ansible ; Kubernetes

.. _ansible_for_kubernetes:

========================
Ansible for kubernetes
========================

.. seealso::

   - https://leanpub.com/ansible-for-kubernetes
