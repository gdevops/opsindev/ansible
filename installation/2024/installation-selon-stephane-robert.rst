
.. _install_stephane_robert_2024:

================================================
Installation d'ansible selon Stéphane Robert
================================================

- https://blog.stephane-robert.info/docs/infra-as-code/gestion-de-configuration/ansible/extension-vscode/


Introduction
================

- :ref:`intro_ansible`

Dans le domaine de l'IT, l'automatisation est devenue essentielle pour gérer
efficacement les configurations et les déploiements à grande échelle.
Ansible, avec sa simplicité d'usage et sa puissance, s'est imposé comme un
outil incontournable.
Dans cette quête d'efficacité, l'environnement de développement joue un rôle
crucial. Visual Studio Code (VSCode), grâce à sa flexibilité et à son écosystème
riche en extensions, offre un cadre idéal pour travailler avec Ansible.

L'extension VSCode pour Ansible enrichit cet environnement en apportant une
série de fonctionnalités spécifiquement conçues pour améliorer le développement
des playbooks et des collections Ansible.

Elle intègre des outils de validation syntaxique, d'autocomplétion intelligente,
et même des recommandations basées sur l'intelligence artificielle avec Ansible Lightspeed,
transformant VSCode en un puissant atelier pour l'automatisation Ansible.


Installation d'Ansible
==============================

Avec Python installé, vous pouvez maintenant installer Ansible.

Ouvrez un terminal et exécutez la commande suivante pour installer Ansible
via pip, le gestionnaire de paquets Python::

    pip install ansible --user

Cette commande installe Ansible et rend ses outils disponibles dans votre terminal.


Installation de l'autocompletion bash
=========================================

Depuis la version :ref:`2.9 d'Ansible <ansible_2_9_0>`, il est possible d'avoir de l'autocompletion
bash des commandes ansible. Pour cela, il suffit d'installer le **package
python argcomplete** :

::

    pip install argcomplete
    activate-global-python-argcomplete


.. _test_ansible:

Test après installation d’Ansible
======================================

Comment exécuter Ansible ? Il suffit de taper la commande suivante qui va
simplement lancer en local (-c local) un shell qui exécutera la commande
echo 'salut B0B':

::

    ansible all -i "localhost," -c local -m shell -a 'echo Salut B0B'

Installation d'ansible-lint
========================================

Pour améliorer la qualité de vos playbooks Ansible, ansible-lint est un outil
indispensable.

Il analyse vos playbooks à la recherche d'erreurs potentielles.

Installez-le en utilisant pipx::

    pip install pipx --user
    pipx install ansible-lint

::

    installed package ansible-lint 24.2.0, installed using Python 3.12.2
    These apps are now globally available
    - ansible-lint
    done! ✨ 🌟 ✨


Installation d'ansible-creator
=====================================

ansible-creator est un outil conçu pour simplifier la création et la gestion
des collections Ansible.

Pour l'installer, utilisez également pipx::

    pipx install ansible-creator


::

    installed package ansible-creator 24.2.0, installed using Python 3.12.2
    These apps are now globally available
    - ansible-creator
    done! ✨ 🌟 ✨



Cet outil vous permettra de structurer efficacement vos collections Ansible
et de les maintenir avec facilité.




Installation de l'Extension VSCode pour Ansible
====================================================

- https://marketplace.visualstudio.com/items?itemName=redhat.ansible
- https://github.com/ansible/vscode-ansible

Une fois l'extension installée, Vscode sera configuré pour reconnaître certains
fichiers Ansible, offrant ainsi une syntaxe en surbrillance, des suggestions
intelligentes et une intégration avec ansible-lint pour la validation de vos
playbooks.

Vous verrez aussi apparaître une icone Ansible dans la barre d'outils :
