.. index::
   pair: Ansible ; 2.9.2 (2019-12-04)

.. _ansible_2_9_2:

=============================================
Ansible 2.9.2 (2019-12-04, "Immigrant Song")
=============================================


.. seealso::

   - https://github.com/ansible/ansible/tree/v2.9.2
   - https://github.com/ansible/ansible/tree/stable-2.9/changelogs
   - https://github.com/ansible/ansible/blob/stable-2.9/changelogs/CHANGELOG-v2.9.rst#release-summary
   - https://groups.google.com/forum/#!topic/ansible-devel/LmSqk0HNlUs
   - https://www.ansible.com/blog/network-features-coming-soon-in-ansible-engine-2.9
