

.. index::
   pair: Ansible ; 2.8.0 (2019-05-16)

.. _ansible_2_8_0:

=============================================
Ansible 2.8.0 (2019-05-16)
=============================================


.. seealso::

   - https://github.com/ansible/ansible/tree/v2.8.0
   - https://github.com/ansible/ansible/blob/devel/docs/docsite/rst/roadmap/ROADMAP_2_8.rst
   - https://github.com/ansible/ansible/projects/30
   - https://github.com/ansible/ansible/blob/v2.8.0/changelogs/CHANGELOG-v2.8.rst
   - https://groups.google.com/forum/#!topic/ansible-announce/OmkhL4zN1oo

