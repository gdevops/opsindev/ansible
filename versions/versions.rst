.. index::
   pair: Ansible ; Versions
   pair: Ansible ; Block


.. _ansible_versions:

=============================================
Versions
=============================================

- https://github.com/ansible/ansible
- https://github.com/ansible/ansible/releases
- https://releases.ansible.com/ansible/
- https://groups.google.com/forum/#!forum/ansible-devel
- https://github.com/ansible/ansible/tree/devel/docs/docsite/rst/roadmap


.. toctree::
   :maxdepth: 3

   2.16/2.16
   2.10.0/2.10.0
   2.9.2/2.9.2
   2.9.0/2.9.0
   2.8.0/2.8.0
   2.7.10/2.7.10
   2.6.0/2.6.0
   2.5.0/2.5.0
   2.4.0/2.4.0
   2.3.0/2.3.0
   2.0.0/2.0.0


