

.. index::
   pair: Ansible ; 2.7.10 (2019-04-04)

.. _ansible_2_7_10:

=============================================
Ansible 2.7.10 (2019-04-04)
=============================================


.. seealso::

   - https://github.com/ansible/ansible/tree/v2.7.10
   - https://github.com/ansible/ansible/blob/devel/docs/docsite/rst/roadmap/ROADMAP_2_7.rst

