
.. _ansible_use_case_2:

===================
Ansible use case 2
===================

.. seealso::

   - :ref:`playbook_prod`

.. toctree::
   :maxdepth: 5

   scripts/scripts
   playbook/playbook
   roles/roles
   vars/vars
   inventory/inventory
