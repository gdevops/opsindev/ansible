.. index::
   pair: Ansible ; Scripts

.. _ansible_scripts:

=============================================
Ansible scripts pour **xxxxxxxxx.yyy.zzz**
=============================================

.. seealso::

   - :ref:`playbook_prod`



Liste des scripts
====================


::

	 189 -rwxrwxrwx 1 root root  373 juin   7 07:46 intranet_dev.bash
	 321 -rwxrwxrwx 1 root root  638 juin   7 07:46 README.md
	 193 -rwxrwxrwx 1 root root  379 avril 25 15:49 transaction_dev.bash
	2049 -rwxrwxrwx 1 root root 1731 juil. 13 12:42 transaction_prod.bash
	 229 -rwxrwxrwx 1 root root  452 juil. 13 12:42 transaction_prod_https.bash
	2049 -rwxrwxrwx 1 root root 1749 juin  12 07:56 transaction_staging.bash
	2049 -rwxrwxrwx 1 root root 1012 juin  12 07:56 update_dev.bash
	2049 -rwxrwxrwx 1 root root 1239 juil.  4 15:22 update_prod.bash
	 225 -rwxrwxrwx 1 root root  442 juin  12 07:56 update_prod_https.bash
	2049 -rwxrwxrwx 1 root root 1248 juin  12 07:56 update_staging.bash
	2049 -rwxrwxrwx 1 root root 1023 juil. 13 12:42 view-license_prod.bash



transaction_prod.bash
============================

.. code-block:: bash
   :linenos:


	#!/bin/bash

	# download last logtransactions_xxxxx.git
	/usr/bin/rm /etc/ansible/roles/django_project/files/transaction/logtransactions_xxxxx.tar.gz
	cd /etc/ansible/roles/django_project/files/transaction/
	/usr/bin/git clone http://gitdeploy:XXXX@gitlab.XXXX/Informatique/logtransactions_xxxxx.git
	cd /etc/ansible/roles/django_project/files/transaction/logtransactions_xxxxx/
	/usr/bin/tar zcvf /etc/ansible/roles/django_project/files/transaction/logtransactions_xxxxx.tar.gz *
	/usr/bin/rm -R /etc/ansible/roles/django_project/files/transaction/logtransactions_xxxxx/

	# download last db
	STATE=$(grep -o "#enable_download_last_db=0" /etc/ansible/playbook/prod/transaction-prod.yml)
	STATE1=$(grep -o "#enable_download_last_db=1" /etc/ansible/playbook/prod/transaction-prod.yml)

	if [ "$STATE" == "#enable_download_last_db=0" ]; then
	 echo "no need to download last transaction db"
	elif [ "$STATE1" == "#enable_download_last_db=1" ]; then
	  echo "db_copy_import role is present in playbook, so need to download last db backup"
	  /usr/bin/rm /etc/ansible/roles/db_copy_import/files/transaction/db.dump.tar.gz
	  /usr/bin/cp /root/scripts/output_backup/db.dump_$(date +%F).tar.gz /etc/ansible/roles/db_copy_import/files/transaction/db.dump.tar.gz
	else
	  echo "bug"
	fi

	ARGS=$*
	PLAY1="/etc/ansible/playbook/prod/transaction-prod.yml"
	INV="/etc/ansible/inventory/hosts"
	TYPE="transaction-prod"

	#Configure app
	cd /etc/ansible
	#/usr/bin/ansible-playbook "$PLAY1" -i "$INV" -e "type="$TYPE"" $ARG
	/usr/bin/ansible-playbook "$PLAY1" -i "$INV" -e "type="$TYPE"" -vvv
	#/usr/bin/ansible-playbook "$PLAY1" -i "$INV" -e "type="$TYPE""
	#/usr/bin/ansible-playbook "$PLAY1" -i "$INV" -e "type="$TYPE"" -vvv --check



transaction_prod_https.bash
============================

.. code-block:: bash
   :linenos:

	#!/bin/bash

	ARGS=$*
	PLAY1="/etc/ansible/playbook/prod/transaction-prod-https.yml"
	INV="/etc/ansible/inventory/hosts"
	TYPE="transaction-prod"

	#Configure app
	cd /etc/ansible
	#/usr/bin/ansible-playbook "$PLAY1" -i "$INV" -e "type="$TYPE"" $ARG
	/usr/bin/ansible-playbook "$PLAY1" -i "$INV" -e "type="$TYPE"" -vvv
	#/usr/bin/ansible-playbook "$PLAY1" -i "$INV" -e "type="$TYPE""
	#/usr/bin/ansible-playbook "$PLAY1" -i "$INV" -e "type="$TYPE"" -vvv --check
