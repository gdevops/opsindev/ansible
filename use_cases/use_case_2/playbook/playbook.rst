.. index::
   pair: Ansible ; Playbooks

.. _ansible_playbooks_2:

=================================================
Ansible playbooks pour **xxxxxxxxx.yyy.zzz**
=================================================

.. seealso::

   - :ref:`ansible_tuto`


::


	├── playbook
	│   ├── dev
	│   │   ├── docker.yml
	│   │   ├── intranet-dev.yml
	│   │   ├── README.md
	│   │   ├── transaction-dev.yml
	│   │   └── update-dev.yml
	│   ├── prod
	│   │   ├── files.id3.eu-https.yml
	│   │   ├── files.id3.eu.yml
	│   │   ├── transaction-prod-https.yml
	│   │   ├── transaction-prod.yml
	│   │   ├── update-prod-https.yml
	│   │   ├── update-prod.yml
	│   │   └── view-license.yml
	│   ├── README.md
	│   └── staging
	│       ├── transaction-staging.yml
	│       └── update-staging.yml



.. toctree::
   :maxdepth: 5


   dev/dev
   staging/staging
   prod/prod
