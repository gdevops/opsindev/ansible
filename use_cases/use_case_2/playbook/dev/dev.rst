.. index::
   pair: dev  ; Ansible


.. _playbook_dev:

========================================================
Déploiement de xxxxxxxxx.yyy.zzz pour le développement
========================================================



Rappel de l'arborescence playbook
===================================

::

	├── ansible.cfg
	├── ansible.cfg.rpmnew
	├── hosts
	├── inventory
	│   └── hosts
	├── playbook
	│   ├── dev
	│   │   ├── docker.yml
	│   │   ├── intranet-dev.yml
	│   │   ├── README.md
	│   │   ├── transaction-dev.yml
	│   │   └── update-dev.yml
	│   ├── prod
	│   │   ├── files.id3.eu-https.yml
	│   │   ├── files.id3.eu.yml
	│   │   ├── transaction-prod-https.yml
	│   │   ├── transaction-prod.yml
	│   │   ├── update-prod-https.yml
	│   │   ├── update-prod.yml
	│   │   └── view-license.yml
	│   ├── README.md
	│   └── staging
	│       ├── transaction-staging.yml
	│       └── update-staging.yml



playbook/dev/transaction-dev.yml
====================================


.. code-block::  yaml

	---
	# playbook.yml
	- hosts : "{{ type }}"
	  remote_user: root
	  vars:
		inventory_hostname: transaction-dev.srv.int.id3.eu
	  pre_tasks:
		- include_vars: /etc/ansible/vars/transaction.yml

	  roles:
		- personnalisation_pvergain
		- base
		- selinux
		- hostname
		#- logwatch
		- db_mariadb
		#- http_apache_server
		- db_copy_import
		#- django_project
