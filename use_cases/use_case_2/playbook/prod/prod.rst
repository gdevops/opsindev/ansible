.. index::
   pair: prod  ; Ansible


.. _playbook_prod:

=================================================
Déploiement de xxxxxxxxx.yyy.zzz en production
=================================================



Rappel de l'arborescence playbook
===================================

::

	├── ansible.cfg
	├── ansible.cfg.rpmnew
	├── hosts
	├── inventory
	│   └── hosts
	├── playbook
	│   ├── dev
	│   │   ├── docker.yml
	│   │   ├── intranet-dev.yml
	│   │   ├── README.md
	│   │   ├── transaction-dev.yml
	│   │   └── update-dev.yml
	│   ├── prod
	│   │   ├── files.id3.eu-https.yml
	│   │   ├── files.id3.eu.yml
	│   │   ├── transaction-prod-https.yml
	│   │   ├── transaction-prod.yml
	│   │   ├── update-prod-https.yml
	│   │   ├── update-prod.yml
	│   │   └── view-license.yml
	│   ├── README.md
	│   └── staging
	│       ├── transaction-staging.yml
	│       └── update-staging.yml



playbook/prod/transaction-prod.yml
====================================


.. code-block::  yaml

	---
	# playbook.yml
	- hosts : "{{ type }}"
	  remote_user: root
	  vars:
		  inventory_hostname: xxxxxxxxx.yyy.zzz
	  pre_tasks:
		- include_vars: /etc/ansible/vars/transaction.yml
	  roles:
		- selinux
		- hostname
		- base
		- configure-mail
		- ansible-role-logwatch
		- db_mariadb
	#enable_download_last_db=0 (1 to activate, 0 do deactivate)
		- db_copy_import
		- http_apache_server
		- django_project
		- {
			role: cron,
			cron_script: 'backup_db_colombie.sh',
			cron_script_j2: 'backup_db_colombie.sh.j2',
			cron_hour: '2',
		  }
		- {
			role: cron,
			cron_script: 'delete_doublons.sh',
			cron_script_j2: 'delete_doublons.sh.j2',
			cron_hour: '3',
		  }
		- {
			role: cron,
			cron_script: 'update_stats_yesterday.sh',
			cron_script_j2: 'update_stats_yesterday.sh.j2',
			cron_hour: '4',
		  }
		- configure-shinken-user
		- ansible-fail2ban
		- {
		   # Vars for lan
		   # =========================================
			role: ansible-role-firewall,
			iptable_1: 'iptables -A INPUT -p tcp --dport 22 -s 185.87.102.242 -j ACCEPT',
			iptable_2: '',
		  }

playbook/prod/transaction-prod-https.yml
==========================================

.. code-block:: yaml

	---
	# playbook.yml
	- hosts : "{{ type }}"
	  remote_user: root
	  vars:
		  inventory_hostname: xxxxxxxxx.yyy.zzz
	  pre_tasks:
		- include_vars: /etc/ansible/vars/transaction.yml
	  roles:
		- ansible-role-certbot
		- https_letsencrypt
