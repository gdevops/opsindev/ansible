.. index::
   pair: Ansible ; inventory

.. _ansible_inventory:

=================================================
Ansible inventory  pour **transaction.id.eu**
=================================================

.. seealso::

   - :ref:`playbook_prod`




Le fichier inventory/hosts
=============================


::

	# xxxxxxxxx.yyy.zzz/transactions colombie notaires
	# ==================================================
	[transaction-prod]
	B.B.B.220

	[transaction-staging]
	X.X.X.202

	[transaction-dev]
	X.X.X.180

	# update.id3.eu/id3programs
	# ==========================
	[update-prod]
	Y.Y.Y.1

	[update-staging]
	X.X.X.203

	[update-dev]
	X.X.X.181

	[updates-prod]
	Z.Z.Z.123

	# docker
	# ==================
	[docker]
	X.X.X.181

	[files.id3.eu]
	192.168.201.4

	# view-license
	# ============
	[view-license-prod]
	X.X.X.22

	# intranet
	# =================
	[intranet-dev]
	X.X.X.184
