.. index::
   pair: Ansible ; vars

.. _ansible_vars:

===========================================
Ansible vars pour **xxxxxxxxx.yyy.zzz**
===========================================

.. seealso::

   - :ref:`playbook_prod`




Fichiers dans vars
===================


::

	2049 -rwxrwxrwx 1 root root  787 juil.  4 15:22 files.yml
	  33 -rwxrwxrwx 1 root root   62 juin   7 07:46 intranet.yml
	2049 -rwxrwxrwx 1 root root 2078 juin  12 07:56 transaction.yml
	2049 -rwxrwxrwx 1 root root 2119 juin  13 09:21 update.yml
	2049 -rwxrwxrwx 1 root root 1706 juil. 13 12:45 view-license.yml


.. _vars_transaction:

vars/transaction.yml
========================

::

	---
	django_rep: '/opt/log_transaction/log_transaction'
	RootName: "log_transaction"
	ProjectDjangoName: "log_transaction"
	python3: '/opt/log_transaction/log_transaction/.venv/bin/python'
	python_wsgi_module: '/opt/log_transaction/log_transaction/.venv/lib/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so'
	python_home: '/opt/log_transaction/log_transaction/.venv'

	# django_project
	# ================
	DATABASE_URL: "mysql://<username>:<password>@127.0.0.1:3306/db_colombie"
	FILE_LOG: "/opt/log_transaction/logs/transaction.log"
	BASIC_AUTH_USER: "<username3>:<password3>"
	ProjectName: "transaction"
	EnvDjangoTemplate: "env_django.j2"
	gitrepo: "http://gitdeploy:<XXX>@gitlab<yyy>/Informatique/<password3>s_colombie.git"
	gitsrc: "/etc/ansible/roles/django_project/files/transaction/<password3>s_colombie.tar.gz"
	gitdest: "/tmp/logtransactions_colombie.tar.gz"
	gitfeature: "master"

	# db_copy_import role
	# =====================
	path_targz: '/tmp/db.dump.tar.gz'
	path_dump: '/tmp/db.dump'
	src_targz: '/etc/ansible/roles/db_copy_import/files/transaction/db.dump.tar.gz'
	mysql_user: "<username>"
	mysql_password: "<password>"
	db_name: "db_colombie"
	mysql_root_password: "<username2>"
	percent_user: "localhost"

	# http_apache_server role
	# =========================
	src_vhost1: "virtualhost_http.conf.j2"
	src_vhost3: "virtualhost_letsencrypt.conf.j2"
	path_vhost: "/etc/httpd/conf.d/log_transaction.conf"
	ApacheServerName:  "vps.hhh.net"
	ApacheServerAlias:  "xxxxxxxxx.yyy.zzz"
	ApacheServerAdmin: "<ddd@ttt>"
	wsgi_process: "transaction_process"
	authuser: "user <username3>"
	url_redirect: "https://xxxxxxxxx.yyy.zzz/"
	htpasswd_user: "<username3>"
	htpasswd_password: "<password3>"

	# https
	# ==================
	certificate_crt: "/etc/letsencrypt/live/xxxxxxxxx.yyy.zzz/cert.pem"
	certificate_key: "/etc/letsencrypt/live/xxxxxxxxx.yyy.zzz/privkey.pem"
	certificate_chain: "/etc/letsencrypt/live/xxxxxxxxx.yyy.zzz/chain.pem"

	# selinux
	# ===================
	selinux: "permissive"
