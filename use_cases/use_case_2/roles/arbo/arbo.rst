.. index::
   pair: Ansible ; Roles

.. _arbo_ansible_roles:

==================================
Arborescence des rôles Ansible
==================================


::

	├── roles
	│   ├── ansible-fail2ban
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── files
	│   │   │   ├── empty
	│   │   │   └── jail.d
	│   │   │       └── sshd.local
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── templates
	│   │   │   └── etc
	│   │   │       └── fail2ban
	│   │   │           ├── fail2ban.local.j2
	│   │   │           └── jail.local.j2
	│   │   ├── tests
	│   │   │   ├── inventory
	│   │   │   ├── test.yml
	│   │   │   └── vagrant.yml
	│   │   ├── Vagrantfile
	│   │   └── vars
	│   │       └── main.yml
	│   ├── ansible-ocsinventory-agent
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── tests
	│   │   │   ├── post_install_test.yml
	│   │   │   ├── requirements.yml
	│   │   │   ├── test.sh
	│   │   │   └── test.yml
	│   │   └── vars
	│   │       ├── Debian.yml
	│   │       ├── main.yml
	│   │       └── RedHat.yml
	│   ├── ansible-role-certbot
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   ├── create-cert-standalone.yml
	│   │   │   ├── include-vars.yml
	│   │   │   ├── install-from-source.yml
	│   │   │   ├── install-with-package.yml
	│   │   │   ├── main.yml
	│   │   │   └── renew-cron.yml
	│   │   ├── tests
	│   │   │   ├── README.md
	│   │   │   ├── requirements.yml
	│   │   │   ├── test-source-install.yml
	│   │   │   ├── test-standalone-nginx-aws.yml
	│   │   │   └── test.yml
	│   │   └── vars
	│   │       ├── default.yml
	│   │       └── Ubuntu-16.04.yml
	│   ├── ansible-role-docker
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── LICENSE
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   ├── docker-compose.yml
	│   │   │   ├── docker-users.yml
	│   │   │   ├── main.yml
	│   │   │   ├── setup-Debian.yml
	│   │   │   └── setup-RedHat.yml
	│   │   └── tests
	│   │       ├── README.md
	│   │       └── test.yml
	│   ├── ansible-role-firewall
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   ├── disable-other-firewalls.yml
	│   │   │   └── main.yml
	│   │   ├── templates
	│   │   │   ├── firewall.bash.j2
	│   │   │   ├── firewall.init.j2
	│   │   │   └── firewall.unit.j2
	│   │   └── tests
	│   │       ├── README.md
	│   │       └── test.yml
	│   ├── ansible-role-logwatch
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── files
	│   │   │   └── ignore.conf
	│   │   ├── LICENSE
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   ├── Debian
	│   │   │   │   └── install.yml
	│   │   │   ├── main.yml
	│   │   │   └── RedHat
	│   │   │       └── install.yml
	│   │   ├── templates
	│   │   │   └── logwatch-conf.j2
	│   │   ├── tests
	│   │   │   └── test.yml
	│   │   └── vars
	│   │       ├── Debian.yml
	│   │       └── RedHat.yml
	│   ├── ansible-role-reboot-and-wait
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   └── tests
	│   │       ├── inventory
	│   │       └── test.yml
	│   ├── base
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   └── tasks
	│   │       └── main.yml
	│   ├── configure-mail
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── tests
	│   │   │   ├── inventory
	│   │   │   └── test.yml
	│   │   └── vars
	│   │       └── main.yml
	│   ├── configure-shinken-user
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── files
	│   │   │   ├── monitoring
	│   │   │   │   ├── check_chrony.sh
	│   │   │   │   ├── check_memory.sh
	│   │   │   │   └── check_postqueue.sh
	│   │   │   └── ssh
	│   │   │       └── monitoring-1.pub
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── tests
	│   │   │   ├── inventory
	│   │   │   └── test.yml
	│   │   └── vars
	│   │       └── main.yml
	│   ├── cron
	│   │   ├── files
	│   │   │   ├── dlegendre_rsa.pub
	│   │   │   └── nopal_rsa.pub
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   └── templates
	│   │       ├── backup_db_colombie.sh.j2
	│   │       ├── backup_db_id3_programs.sh.j2
	│   │       ├── delete_doublons.sh.j2
	│   │       ├── import_id3web.bash.j2
	│   │       └── update_stats_yesterday.sh.j2
	│   ├── db_copy_import
	│   │   ├── files
	│   │   │   ├── transaction
	│   │   │   │   └── db.dump.tar.gz
	│   │   │   ├── update
	│   │   │   │   ├── id3_programs.tar.gz
	│   │   │   │   └── id3web.tar.gz
	│   │   │   └── view-license
	│   │   │       ├── db_view-license.tar.gz
	│   │   │       └── id3web_full.tar.gz
	│   │   ├── README.md
	│   │   └── tasks
	│   │       └── main.yml
	│   ├── db_mariadb
	│   │   ├── files
	│   │   │   ├── MariaDB.cnf
	│   │   │   └── MariaDB.repo
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   └── templates
	│   │       ├── disable-binary-logging.cnf
	│   │       └── my.cnf.j2
	│   ├── db_sql_import
	│   │   ├── files
	│   │   │   └── update
	│   │   │       ├── id3view.sql
	│   │   │       └── update_programs.sql
	│   │   ├── README.md
	│   │   └── tasks
	│   │       └── main.yml
	│   ├── django_project
	│   │   ├── files
	│   │   │   ├── transaction
	│   │   │   │   └── logtransactions_colombie.tar.gz
	│   │   │   ├── update
	│   │   │   │   └── update_id3_eu.tar.gz
	│   │   │   └── view-license
	│   │   │       └── view_license.tar.gz
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── templates
	│   │   │   ├── env_django2.j2
	│   │   │   └── env_django.j2
	│   │   └── vars
	│   │       └── main.yml
	│   ├── hostname
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── tests
	│   │   │   ├── inventory
	│   │   │   └── test.yml
	│   │   └── vars
	│   │       └── main.yml
	│   ├── http_apache_server
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   └── templates
	│   │       └── virtualhost_http.conf.j2
	│   ├── http_apache_server_no_htaccess
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   └── templates
	│   │       └── virtualhost_http.conf.j2
	│   ├── https_letsencrypt
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── templates
	│   │   │   ├── ssl.conf.j2
	│   │   │   └── virtualhost_letsencrypt.conf.j2
	│   │   ├── tests
	│   │   │   ├── inventory
	│   │   │   └── test.yml
	│   │   └── vars
	│   │       └── main.yml
	│   ├── https_self_signed
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── files
	│   │   │   ├── transaction
	│   │   │   │   ├── xxxxxxxxx.yyy.zzz.crt
	│   │   │   │   ├── xxxxxxxxx.yyy.zzz.csr
	│   │   │   │   └── xxxxxxxxx.yyy.zzz.key
	│   │   │   └── update
	│   │   │       ├── update.id3.eu.crt
	│   │   │       ├── update.id3.eu.csr
	│   │   │       └── update.id3.eu.key
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── templates
	│   │   │   └── transaction.local.conf.j2
	│   │   ├── tests
	│   │   │   ├── inventory
	│   │   │   └── test.yml
	│   │   └── vars
	│   │       └── main.yml
	│   ├── personnalisation_pvergain
	│   │   ├── files
	│   │   │   └── config.fish
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   └── vars
	│   │       └── main.yml
	│   ├── README.md
	│   ├── selinux
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── meta
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── tests
	│   │   │   ├── inventory
	│   │   │   └── test.yml
	│   │   └── vars
	│   │       └── main.yml
	│   ├── ssh_keys
	│   │   ├── files
	│   │   │   ├── dlegendre_rsa.pub
	│   │   │   ├── dockerdeploy_rsa.pub
	│   │   │   ├── nforey_rsa.pub
	│   │   │   ├── plartigue_rsa.pub
	│   │   │   └── rduchet_rsa.pub
	│   │   └── tasks
	│   │       └── main.yml
	│   └── vmware-deploy
	│       ├── defaults
	│       │   └── main.yml
	│       ├── handlers
	│       │   └── main.yml
	│       ├── meta
	│       │   └── main.yml
	│       ├── README.md
	│       ├── tasks
	│       │   └── main.yml
	│       ├── tests
	│       │   ├── inventory
	│       │   └── test.yml
	│       └── vars
	│           └── main.yml
