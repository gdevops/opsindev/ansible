.. index::
   pair: Ansible ; django_project role

.. _ansible_django_project:

=====================
django_project role
=====================



Extrait de l'arborescence des rôles
======================================


::

	│   ├── django_project
	│   │   ├── files
	│   │   │   ├── transaction
	│   │   │   │   └── logtransactions_colombie.tar.gz
	│   │   │   ├── update
	│   │   │   │   └── update_id3_eu.tar.gz
	│   │   │   └── view-license
	│   │   │       └── view_license.tar.gz
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   ├── templates
	│   │   │   ├── env_django2.j2
	│   │   │   └── env_django.j2



roles/django_project/tasks/main.yml
======================================


.. code-block:: yaml
   :linenos:

	---
	# roles/django_staging/tasks/main.yml
	# =========================================


	# https://docs.ansible.com/ansible/service_module.html
	# Stop service httpd, if running
	- name: Stop Apache HTTP server
	  service:
		name: httpd
		state: stopped


	- name: Delete the directory /opt/{{ RootName }}/
	  # https://docs.ansible.com/ansible/file_module.html
	  file:
		path: /opt/{{ RootName }}
		state: absent
	  become: true

	- name: Delete the directory /opt/checkout/
	  # https://docs.ansible.com/ansible/file_module.html
	  file:
		path: /opt/checkout
		state: absent
	  become: true

	## https://docs.ansible.com/ansible/git_module.html
	#- name: Get the feature branch with git
	#  git:
	#    repo: '{{ gitrepo }}'
	#    #repo: "ssh://git@gitlab.srv.int.id3.eu/Informatique/logtransactions_colombie.git"
	#    dest: /opt/checkout
	#    # https://delicious-insights.com/fr/articles/git-workflows-parallel-features/
	#    version: '{{ gitfeature }}'
	#
	- name: Création du répertoire /opt/{{ RootName }}
	  # https://docs.ansible.com/ansible/file_module.html
	  file:
		path: /opt/{{ RootName }}
		state: directory
		mode: 0755
	  become: true

	- name: Delete the directory /opt/checkout/
	 # https://docs.ansible.com/ansible/file_module.html
	  file:
		path: /opt/checkout
		state: directory
	  become: true

	# https://docs.ansible.com/ansible/copy_module.html
	- name: Copy "{{ gitsrc }}"  -->  "{{ gitdest }}"
	  copy:
		src: "{{ gitsrc }}"
		dest: "{{ gitdest }}"

	- name: Unarchive "{{ gitdest }}" --> /opt/checkout
	  unarchive:
		src: "{{ gitdest }}"
		dest: /opt/checkout
		remote_src: yes

	# Copie des sources django
	- name: Copie des sources du projet django sous /opt/{{ RootName }}/{{ ProjectDjangoName }}
	  # https://docs.ansible.com/ansible/command_module.html
	  command:  cp -rfu /opt/checkout/django-www/{{ ProjectName }}/. /opt/{{ RootName }}/{{ ProjectDjangoName }}
	  become: true


	- name: Le propriétaire de /opt/{{ RootName }}/{{ ProjectDjangoName }} est apache:apache
	  # https://docs.ansible.com/ansible/file_module.html
	  file:
		path: /opt/{{ RootName }}/{{ ProjectDjangoName }}
		state: directory
		recurse: yes
		owner: apache
		group: apache
		mode: 0755
	  become: true

	- name: Création du répertoire /opt/{{ RootName }}/static
	  file:
		path: /opt/{{ RootName }}/static
		state: directory
		owner: apache
		group: apache
		mode: 0755
	  become: true


	# Création du répertoire /opt/log_transaction/logs utilisé par le serveur apache
	# ErrorLog    /opt/log_transaction/logs/error_log
	# CustomLog   /opt/log_transaction/logs/access_log combined
	- name: Création du répertoire /opt/{{ RootName }}/logs
	  # https://docs.ansible.com/ansible/file_module.html
	  file:
		path: /opt/{{ RootName }}/logs
		state: directory
		owner: apache
		group: apache
		mode: 0755
	  become: true

	- name: Création du fichier /opt/{{ RootName }}/logs/{{ ProjectName }}.log
	  # https://docs.ansible.com/ansible/file_module.html
	  file:
		path: /opt/{{ RootName }}/logs/{{ ProjectName }}.log
		owner: apache
		group: apache
		state: touch
		mode: 0777
	  become: true


	# https://docs.ansible.com/ansible/command_module.html
	- name: Install django + django modules
	  command: pipenv install --python /usr/bin/python3.6
	  args:
		  chdir: /opt/{{ RootName }}/{{ ProjectDjangoName }}
	  environment:
		# create the python virtual environment under
		# /opt/log_transaction/log_transaction/.venv
		# The python3.6 interpreter is here
		# /opt/log_transaction/log_transaction/.venv/bin/python
		# https://docs.pipenv.org/advanced/#custom-virtual-environment-location
		PIPENV_VENV_IN_PROJECT: 1
		# Authentification basique avec Django : https://github.com/mvantellingen/wsgi-basic-auth
		WSGI_AUTH_CREDENTIALS: "{{ BASIC_AUTH_USER }}"


	# https://docs.ansible.com/ansible/command_module.html
	- name: Le propriétaire de /opt/{{ RootName }}/{{ ProjectDjangoName }}/.venv est apache:apache
	  file:
		path: /opt/{{ RootName }}/{{ ProjectDjangoName }}/.venv
		state: directory
		recurse: yes
		owner: apache
		group: apache
		mode: 0755
	  become: true


	- name: Mise en place du fichier .env
	  # https://docs.ansible.com/ansible/template_module.html
	  template:
		src: "{{ EnvDjangoTemplate }}"
		dest: /opt/{{ RootName }}/{{ ProjectDjangoName }}/config/settings/.env
		owner: apache
		group: apache
		mode: "u=r,g=r,o=r"
	  become: yes


	- name: Déploiement des static sous /opt/{{ RootName }}/static
	  # http://docs.ansible.com/ansible/django_manage_module.html
	  command: pipenv run python manage_prod.py collectstatic --noinput
	  args:
		  chdir: /opt/{{ RootName }}/{{ ProjectDjangoName }}


	- name: Migration éventuelle de certaines tables
	  # http://docs.ansible.com/ansible/django_manage_module.html
	  command: pipenv run python manage_prod.py migrate
	  args:
		  chdir: /opt/{{ RootName }}/{{ ProjectDjangoName }}


	# https://docs.ansible.com/ansible/service_module.html
	# Redémarrage du service httpd
	- name: Start Apache HTTP server
	  service:
		name: httpd
		state: started
		# activation au démarrage
		enabled: yes
	  notify:
	   # redémarrer le serveur HTTP
	  - restart httpd
