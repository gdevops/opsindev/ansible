.. index::
   pair: Ansible ; HTTP Apache role
   pair: HTTP ; Virtual Host

.. _ansible_http_apache:

=====================
HTTP Apache role
=====================



Extrait de l'arborescence des rôles
======================================


::


	│   ├── http_apache_server
	│   │   ├── defaults
	│   │   │   └── main.yml
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── README.md
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   └── templates
	│   │       └── virtualhost_http.conf.j2



roles/http_apache_server/tasks/main.yml
==========================================


.. code-block:: yaml
   :linenos:

	---
	# roles/http_apache_server/tasks/main.yml
	# ========================================

	# https://docs.ansible.com/ansible/yum_module.html
	- name: Installation du serveur HTTP apache (https://en.wikipedia.org/wiki/Apache_HTTP_Server)
	  yum: name={{ item }} state=present
	  with_items:
		- httpd
		- httpd-devel
		- python-passlib

	- name: Start and enable Apache HTTP server
	  service: name=httpd  state=started enabled=yes

	- name: Create directory /etc/httpd/passwd
	  file:
		path: /etc/httpd/passwd/
		state: directory
		owner: root
		group: root
		mode: 0755
	  become: true

	# python passlib is required
	- name: Add "{{ htpasswd_user }}" in .htpasswd file
	  htpasswd:
		path: /etc/httpd/passwd/.htpasswd
		name: "{{ htpasswd_user }}"
		password: "{{ htpasswd_password }}"
		owner: root
		group: root
		mode: 0644

	- name: Mise en place du fichier virtual host pour Apache 2.4
	  #https://docs.ansible.com/ansible/template_module.html
	  template:
			src: "{{ src_vhost1 }}"
			dest: "{{ path_vhost }}"
			owner: root
			group: root
			mode: "u=r,g=r,o=r"
	  become: yes




roles/http_apache_server/templates/virtualhost_http.conf.j2
===============================================================

::

	<VirtualHost *:80>
		ServerName  {{ ApacheServerName }}
		ServerAlias {{ ApacheServerAlias }}
		ServerAdmin {{ ApacheServerAdmin }}
		DocumentRoot   /opt/{{ RootName }}/{{ ProjectDjangoName }}
		ErrorLog       /opt/{{ RootName }}/logs/error_log
		CustomLog      /opt/{{ RootName }}/logs/access_log combined

		Alias /static/ /opt/{{ RootName }}/static/
		Alias /media/ /opt/{{ RootName }}/media/

		LoadModule wsgi_module  {{ python_wsgi_module }}

		WSGIDaemonProcess  {{ wsgi_process }} \
			user=apache group=apache threads=5 \
			python-home=/opt/{{ RootName }}/{{ ProjectDjangoName }}/.venv \
			python-path=/opt/{{ RootName }}/{{ ProjectDjangoName }}

		WSGIScriptAlias / /opt/{{ RootName }}/{{ ProjectDjangoName }}/config/wsgi.py \
			process-group={{ wsgi_process }} \
			application-group= %{GLOBAL}

		<Directory /opt/{{ RootName }}>
			AuthType                Basic
			AuthName                "Restricted Files"
			AuthBasicProvider       file
			AuthUserFile            /etc/httpd/passwd/.htpasswd
			Require                 {{ authuser }}
		</Directory>

	</VirtualHost>
