.. index::
   pair: Ansible ; db_mariadb role

.. _ansible_db_mariadb:

=====================
db_mariadb role
=====================



Extrait de l'arborescence des rôles
======================================


::


	│   ├── db_mariadb
	│   │   ├── files
	│   │   │   ├── MariaDB.cnf
	│   │   │   └── MariaDB.repo
	│   │   ├── handlers
	│   │   │   └── main.yml
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   └── templates
	│   │       ├── disable-binary-logging.cnf
	│   │       └── my.cnf.j2



roles/db_mariadb/tasks/main.yml
======================================


.. code-block:: yaml
   :linenos:

	---
	# roles/db_mariadb/tasks/main.yml
	# =================================

	# https://galaxy.ansible.com/mamercad/mariadb-galera/
	# https://github.com/mamercad/ansible-mariadb-galera/blob/master/tasks/main.yml
	#
	# https://docs.ansible.com/ansible/copy_module.html
	- name: deploy MariaDB.repo
	  copy: src=MariaDB.repo dest=/etc/yum.repos.d/MariaDB.repo owner=root group=root mode=0644


	# https://docs.ansible.com/ansible/rpm_key_module.html
	- name: import RPR-GPG-KEY-MariaDB
	  rpm_key: key=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB state=present

	# https://docs.ansible.com/ansible/yum_module.html
	- name: Installation de MariaDB-server et MariaDB-client
	  yum: name={{ item }} state=present
	  with_items:
		- MariaDB-server
		- MariaDB-client

	# Ansible requires the MySQLdb Python package on the remote host
	- name: Installation de MySQL-python
	  yum: name=MySQL-python state=present

	# https://docs.ansible.com/ansible/service_module.html
	- name: start and enable mysqld
	  service: name=mysql state=started enabled=yes


	# https://docs.ansible.com/ansible/shell_module.html
	# https://mariadb.com/kb/en/mariadb/time-zones/#mysql-time-zone-tables
	- name: update the mysql.time_zone table
	  shell: mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root mysql


	# https://docs.ansible.com/ansible/copy_module.html
	- name: Copie de MariaDB.cnf -> /etc/my.cnf.d/server.cnf
	  copy: src=MariaDB.cnf dest=/etc/my.cnf.d/server.cnf
	  notify:
		- restart mysql

	# https://docs.ansible.com/ansible/file_module.html
	- name: Create the log directory --> /var/log/mariadb
	  file:
		path: /var/log/mariadb
		state: directory
		mode: 0755

	- name: Disable MariaDB binary logging -> /etc/my.cnf.d/disable-binary-logging.cnf
	  template:
		src: disable-binary-logging.cnf
		dest: /etc/my.cnf.d/disable-binary-logging.cnf
		owner: root
		group: root
	  notify: restart mysql

	- name: Copy .my.cnfj2 file with root password credentials --> ~/.my.cnf.
	  template:
		src: my.cnf.j2
		dest: ~/.my.cnf
		owner: root
		group: root
		mode: 0600

	# https://docs.ansible.com/ansible/mysql_user_module.html
	- name: Set root user password
	  mysql_user:
		name: root
		host: "{{ item }}"
		password: "{{ mysql_root_password }}"
		check_implicit_admin: yes
		priv: "*.*:ALL,GRANT"
		state: present
	  with_items:
		- "{{ inventory_hostname }}"
		- ::1
		- "{{ percent_user }}"
		- localhost
		- 127.0.0.1

	# https://github.com/bertvv/ansible-role-mariadb/blob/master/tasks/users.yml
	- name: remove anonymous users
	  mysql_user:
		name: ''
		login_user: root
		host_all: yes
		state: absent

	- name: Remove the test database
	  mysql_db:
		name: test
		state: absent
