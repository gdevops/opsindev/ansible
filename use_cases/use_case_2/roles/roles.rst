.. index::
   pair: Ansible ; Roles

.. _ansible_roles_2:

============================================
Ansible roles pour **xxxxxxxxx.yyy.zzz**
============================================

.. seealso::

   - :ref:`playbook_prod`


.. toctree::
   :maxdepth: 3

   arbo/arbo
   cron/cron
   db_mariadb/db_mariadb
   django_project/django_project
   http_apache_server/http_apache_server
   https_letsencrypt/https_letsencrypt
