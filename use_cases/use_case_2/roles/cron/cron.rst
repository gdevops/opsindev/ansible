.. index::
   pair: Ansible ; cron

.. _ansible_cron:

=======================================
cron role pour **xxxxxxxxx.yyy.zzz**
=======================================

.. seealso::

   - :ref:`ansible_vars`




Extrait de l'arborescence des rôles
======================================


::


	│   ├── cron
	│   │   ├── files
	│   │   │   ├── xxx_rsa.pub
	│   │   │   └── yyy_rsa.pub
	│   │   ├── tasks
	│   │   │   └── main.yml
	│   │   └── templates
	│   │       ├── backup_db_xxxx.sh.j2
	│   │       ├── backup_db_aaa_pppppp.sh.j2
	│   │       ├── delete_doublons.sh.j2
	│   │       ├── import_db.bash.j2
	│   │       └── update_stats_yesterday.sh.j2



roles/cron/tasks/main.yml
======================================


.. code-block:: yaml
   :linenos:

	---
	# roles/cron/tasks/main.yml
	# ==============================

	- name: Create folder /root/scripts/
	  # https://docs.ansible.com/ansible/file_module.html
	  file:
		path: /root/scripts/
		state: directory
		mode: 0755
	  become: true


	- name: Create folder /root/scripts/output_backup
	  # https://docs.ansible.com/ansible/file_module.html
	  file:
		path: /root/scripts/output_backup
		state: directory
		mode: 0755
	  become: true

	# =================================================================
	- name: Mise en place du script {{ cron_script }}
	  # https://docs.ansible.com/ansible/template_module.html
	  template:
		src: "{{ cron_script_j2 }}"
		dest: /root/scripts/{{ cron_script }}
		mode: "u+x,g-wx,o-rwx"
	  become: yes

	- cron:
		# https://docs.ansible.com/ansible/latest/modules/cron_module.html
		name: Add {{ cron_script}} to crontab at {{ cron_hour }} hours
		minute: "0"
		hour: "{{ cron_hour }}"
		job: "/root/scripts/{{ cron_script }} > /dev/null 2>&1"


	- name: "Add SSH Key"
	  authorized_key: user=root key="{{lookup('file', item )}}" state=present
	  with_items:
		- /etc/ansible/roles/cron/files/xxx_rsa.pub
		- /etc/ansible/roles/cron/files/yyy_rsa.pub



roles/cron/template/delete_doublons.sh.j2
=============================================

::

    {{ python3 }} {{ django_rep }}/manage_prod.py delete_doublons



roles/cron/template/backup_db_xxxx.sh.j2
===========================================

::

	#!/bin/bash
	# Output Backup
	cd /root/scripts/output_backup;

	/usr/bin/rm /root/scripts/output_backup/db.dump.tar.gz

	# MySQL Dump
	mysqldump -u root -p'<password>' --databases db_xxxx > db.dump;

	tar zcvf db.dump.tar.gz db.dump
	/usr/bin/rm /root/scripts/output_backup/db.dump


roles/cron/template/backup_db_xxxx.sh.j2
===========================================

::

	#!/bin/bash
	# Output Backup
	cd /root/scripts/output_backup;

	/usr/bin/rm /root/scripts/output_backup/aaa_pppppp.tar.gz
	mysqldump -u root -p'<password>' --databases db_aaa_pppppp > aaa_pppppp.dump;
	tar zcvf aaa_pppppp.tar.gz aaa_pppppp.dump
	/usr/bin/rm /root/scripts/output_backup/aaa_pppppp.dump


roles/cron/template/import_db.sh.j2
===========================================

::

    #!/bin/bash

	scp root@XX.XX.XX.3:/root/CRONTAB/output_backup/db2_XXX_full.tar.gz /tmp/
	cd /tmp
	tar xvf /tmp/db2_XXX_full.tar.gz
	mysql --user=root --password=<password> db_yyyy < /tmp/db2_XXX_full.dump
