

.. index::
   pair: Ansible ; Playbooks


.. _ansible_playbooks:

=============================================
Ansible playbooks
=============================================

.. seealso::

   - https://docs.ansible.com/ansible/latest/playbooks.html





.. _playbook_1:

playbook.yml example 1
=========================

.. seealso::

   - :ref:`role_base`
   - :ref:`role_python36`
   - :ref:`role_apache_http_server`


.. code-block:: yaml

    ---
    # playbook.yml
    - hosts : staging
      remote_user: root
      roles:
        - base
        - python36
        - http_apache_server
