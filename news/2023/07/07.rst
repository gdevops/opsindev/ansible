.. index::
   ! JetPorch
   ! Jet

.. _ansible_2023_07:

=======================
2023-07
=======================


2023-07-31 Le créateur d’Ansible dévoile son projet Jet, une alternative à… Ansible, codée en Rust !  (projet abandonné)
===============================================================================================================================

- https://www.jetporch.com/ (projet abandonné)
- https://www.it-connect.fr/le-createur-dansible-devoile-son-projet-jet-une-alternative-a-ansible-codee-en-rust/
- https://www.jetporch.com/community/discord-chat

Le créateur d'Ansible, Michael DeHaan, a pour ambition de créer un nouvel
outil d'automatisation codé avec le langage Rust ! Voici ce que l'on sait !

Lorsque l'on parle d'automatisation, Ansible est une solution incontournable !
À l'origine de cet outil, Michael DeHaan qui a fondé la société Ansible
en 2012, avant qu'elle soit rachetée par Red Hat en 2015.

Avec Ansible, son ambition est claire : nous permettre d'automatiser les
configurations des serveurs, que ce soit pour 10 serveurs, ou plusieurs
milliers.

Une mission accomplie tant cet outil est devenu une référence pour les DevOps.

Toutefois, Michael DeHaan estime qu'il y a encore de la place pour mettre
sur le marché d'autres solutions d'automatisation !

Pour preuve, il vient de lancer un nouveau projet pour créer un nouvel
outil d'automatisation basé sur le langage Rust, très en vogue.
D'ailleurs, au passage, Microsoft a commencé aussi à adopter ce langage
pour réécrire en Rust une partie du noyau de Windows 11.

Bien qu'il aurait pu utiliser le langage Go, Michael estime que le système
de types de Rust est plus beau donc il a retenu ce langage : "Une nouvelle
plateforme d'automatisation implémentée en Rust, avec un code extrêmement
lisible.", écrit-il au moment de présenter ce nouveau projet.

Alors qu'il n'avait pas dévoilé le nom initialement, Michael DeHaan a mis
à jour son billet pour ajouter un lien vers le site "jetporch.com" afin
de présenter le projet Jet : "Jet Enterprise Performance Orchestrator, aka 'jet'."

Sur ce nouveau site, il explique qu'il veut faire de Jet une plateforme
d'automatisation et d'orchestration moderne pour les entreprises, basée
sur la fiabilité, un code propre et la sécurité avec une vraie stratégie
d'audit du code.

Il souhaite aussi que Jet soit "rapide comme l'éclair" à l'exécution et
qu'il soit déployé à l'échelle planétaire. Prometteur !

Si vous utilisez Ansible, il vous sera possible de tester Jet assez
facilement, car la structure de leur playbook sera similaire : "Pour
faciliter la tâche des utilisateurs informatiques disposant d'un contenu
existant, jet utilisera un dialecte YAML très similaire au langage
Ansible playbook, et sera en mesure d'exécuter de nombreux, voire la
plupart des modules Ansible existants à l'aide d'une simple couche
d'encapsulation." -

Il parle d'une compatibilité comprise entre 90 et 95% avec les playbooks existants.

Un serveur Discord est déjà à disposition de la communauté ! Même si Jet
n'en est qu'au début de son développement, Michael prévoit d'avancer très rapidement.

Qu'en pensez-vous ?

2023-07- A New IT Automation Project? Moving Beyond Ansible And Keeping The Spirit - An Invitation
=======================================================================================================

- https://laserllama.substack.com/p/a-new-it-automation-project-moving
- https://www.jetporch.com/

update: whoa, this got popular! if you are reading this, check out
https://jetporch.com for the start of the homepage (no code shared just yet)
and development blog and Discord Chat information.

Both links are at the bottom of the page.

Hi everyone!

So you know Ansible, right? I started the project in 2012. It's now
possibly a $600M/year business for IBM and runs computing fleets in most
of the Fortune 500. I would not have imagined that growth when I started.
I am now looking for a new project, and already have the idea.

So, what’s the problem I want to solve?

Ansible was originally successful because it started out as a really
minimal system. It's now successful for different reasons — one of those
is it’s everywhere!
While much of my original design and influence is still present, I also
see things I would do differently, and I think that’s a fair thing to say,
without discounting any of the hard work Red Hat has put into it and
especially spreading it everywhere, which is appreciated.

The challenge is this - it's now 2023, and because automation became easy,
businesses everywhere used that to increase their IT scale and complexity -
as they rightfully should have.

We now need tools that are more scalable and also, even easier to keep
up with that demand. I would like to see a move back towards minimalism,
grocery-list style simplicity, and looking at things through a more
systems-programming / hard-engineering type lens.

Further, with Puppet and Chef being acquired and essentially taken out
of the game, we need new places to try new ideas.

Goals? Simply put, things should be rock-solid, lightening fast, well-engineered,
and readable and auditable by people who have never even seen the content before.

Nobody wants to learn a new language.

Folks have a lot of existing automation content. They don’t want to port
content to a new system, and they have even less time for something that
might feel like programming.

People also didn’t want to help build something from scratch and don’t
have a lot of time to contribute to a project like they did in 2012
before agile ate IT/DevOps.
So, from experiences in trying projects after 2015, I know I need to
solve these problems and release out of the gate with a focus on compatibility
and deliver a solution that completely solves some basic use cases.
Things must look like a product on launch, and they will!

Here’s my idea:

- A new automation platform implemented in Rust, with extremely literate code.
  Rust does not have objects, but structs and traits are essentially
  the same thing and just as good!
  I know Go is popular, **but the type system of Rust is more beautiful**.
  Both produce single-binary executables. I’m happy to help people learn
  the code and ecosystem as it evolves.
- Support local connectivity, SSH, and also new message bus deployment
  architectures supporting potentially 50k-100k systems at once.
  This architecture will get built shortly after initial release.
- Optional, fully asynchronous modes of operation enabled by threading
  architectures of Rust + messaging, with configurable parallelism at
  different parts of the pipeline to manage resource contention
- Aiming for roughly 90-95% compatibility with existing playbooks,
  including all critical language features.
- Ability to execute existing Ansible python modules via a language wrapper,
  transparently. As such, we may ship with a bare minimum of modules at
  the first release. I’m also very open to allowing execution of modules
  from other configuration management systems or even hooking into Terraform
  possibly and exposing those modules in a different way. TBD!
- Amazing, detailed yet concise documentation with examples
- Supporting major Linux distributions at the outset and possibly other
  Unixes. Take advantage of the advantages of these superior platforms.
- Easy to run even from a Mac and git checkout
- Regular stable releases but also easy to follow the development branch
- Very strong commitment to a stable language specification
- Everything can be mostly run as an open source project like Ansible
  was in 2012. We’ll have a blog, a mailing list / google group, and a
  minimum of repositories.
- Legal note: Ansible is a registered trademark of Red Hat / IBM and is
  GPLv2 licensed without copyright assignment.
  This new effort would be GPLv2 so there wouldn’t be any problems with
  reading code that I and others have contributed.
- People are going to say this effort is a ton of work.
  It’s not terrible, really.
  I had the same conclusions drawn when I wanted to have alternatives to
  Puppet in 2012. We had a working implementation in months that was
  field usable!
  We start small and we grow. And we build something for posterity that
  is extremely easy to use and designed for the scale and software
  diversity of 2023.
- People may say containers ate the configuration management world.
  They did! But this market is not dead, it’s still a billion plus
  dollar business.

What I want to do is build a solution that allows for systems to be managed
in classic ways, including the container supporting infrastructure and
stateful data layers, as well as allowing people to also embrace the
simplicity of also managing non-container cloud and datacenter instances
directly.

I want to build a system that allows general purpose workflow orchestration
as well.

It should also be used to produce images of multiple different types, or
audit systems to make sure they match configuration policy.
We’ll start small, but the sky is pretty much the limit.
